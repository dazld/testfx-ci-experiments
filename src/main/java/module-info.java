module org.example.testfxciexperiments {
    requires javafx.controls;
    requires javafx.fxml;
    
    
    opens org.example.testfxciexperiments to javafx.fxml;
    exports org.example.testfxciexperiments;
}